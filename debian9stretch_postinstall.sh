#!/bin/bash
# version 1.8

# A rajouter/modifier :
# - Prevoir nouveau thème aussi pour Gnome (pas uniquement xfce)

########################################
# SCRIPT POST-INSTALL DEBIAN 9 STRETCH #
########################################

# Condition minimum requise avant de lancer ce script :
# - Avoir Debian 9 Stretch X64 d'installé (au moins système de base)
# - Ne pas avoir touché au sources.list
# (il n'est pas obligatoire d'avoir déjà un environneemnt de bureau déjà installé)

##################################################################
# Vérification que le script est bien lancé avec les droits admin
##################################################################
if [ "$UID" -ne "0" ]
then
  echo "Vous n'avez pas les bons droits, merci de lancer le script avec sudo !"
  exit 
fi 

# Question 1
echo "======================================="
echo "Veuillez choisir le profil de base a utiliser"
echo "======================================="
echo "1 = Perso"
echo "2 = Pro - technicien/ingénieur/dev"
echo "======================================="
read -p "Répondre par le chiffre correspondant (1 ou 2) : " choix_profil
clear

# Question 2
echo "======================================="
echo "Faut t'il backporter LibreOffice ?"
echo "======================================="
echo "1 = Non, conserver la version des dépots (5.2)"
echo "2 = Oui, utiliser la version du dépot Stretch-backport (=> pas encore actif pour l'instant !)"
echo "======================================="
read -p "Répondre par le chiffre correspondant (1 ou 2) : " choix_libreoffice
clear

# Question 3
echo "======================================="
echo "Faut t'il installer un environnement graphique de bureau ?"
echo "======================================="
echo "1 = non, j'ai déja un environnement de bureau"
echo "2 = Xfce avec extra (+LightDM) + thème/icone supplémentaire"
echo "3 = Mate avec extra (+LightDM)"
echo "4 = Gnome Shell/Gnome 3 (+GDM3)"
echo "5 = Kde/Plasma 5 (+SDDM)"
echo "6 = Cinnamon (+LightDM)"
echo "======================================="
read -p "Répondre par le chiffre correspondant (1 a 6) : " choix_environnement
clear

# Question 4
echo "======================================="
echo "Un (ou plusieurs) logiciels supplémentaires a installer ?"
echo "======================================="
echo "1 = Non"
echo "2 = Oracle Java (version propriétaire)"
echo "3 = Teamviewer : dernière version (V12 a ce jour)"
echo "4 = Teamviewer V8 (pour tech assistance)"
echo "5 = Google Earth 64 bits"
echo "6 = Pack Developpeur : Android Studio + Bluefish + Emacs + Sublime Text"
echo "7 = Google Chrome"
echo "8 = Client Hubic"
echo "9 = Minecraft"
echo "10 = Pour la lecture DVD commerciaux"
#echo "11 = Activer support de Snappy & Flatpak"
echo "12 = Activer Compton (no tearing xfce/mate/lxde) => inutile si chipset graphique intel présent !"
echo "13 = Pack Gaming : Steam + PlayOnLinux + Wine + Minetest + SuperTux + SuperTuxKart + OpenArena"
echo "14 = Skype for Linux X64 (Beta 5.3)"
echo "15 = PyCharm - Community Edition"
echo "100 = Supplément perso Simon => (driver carte wifi MT7630E pour mon Asus15p)"
echo "======================================="
read -p "Répondre par le ou les chiffres correspondants séparés d'un espace (exemple : 2 5 6) : " choix_supplement
clear

# Question 5
echo "======================================="
echo "Faut t'il installer les drivers propriétaires ?"
echo "======================================="
echo "1 = Non"
echo "2 = Oui, driver propriétaire nvidia récent (carte nvidia unique & récente)"
echo "3 = Oui, driver opensource+propriétaire intel (carte intel seule)"
echo "4 = Oui, technologie Optimus : carte nvidia + chipset nvidia (bumblebee)"
echo "5 = Additions invités pour VirtualBox" 
echo "6 = Oui, driver propriétaire nvidia ancien (carte nvidia unique & ancienne : pour pc travail par ex)"
echo "======================================="
read -p "Répondre par le chiffre correspondant (1 a 6) : " choix_driver
clear

# Question 6
echo "======================================="
echo "Identifiant sous Debian" 
echo "======================================="
read -p "Merci de saisir votre login utilisateur : " mon_login
clear

# Modification sources.list
wget --no-check-certificate https://raw.githubusercontent.com/sibe39/linux_distrib/master/debian9-sources.list 
mv -f debian9-sources.list /etc/apt/sources.list
apt update 
#apt install --force-yes -y pkg-mozilla-archive-keyring #<= plus tard pour backport mozilla

# Ajout multiarch (pour pouvoir installer des dépendances 32 bits, obligatoire avec certains paquets)
dpkg --add-architecture i386

# Vérification que le système est a jour 
apt update
apt full-upgrade -y

#############################################
# Installation de l'environnement de bureau #
#############################################

#[ Xfce ]
if [ "$choix_environnement" = "2" ] ; then
  apt install -y xfce4 xfce4-goodies xfce4-artwork xfce4-terminal xfce4-weather-plugin xfce4-whiskermenu-plugin xfce4-mixer xfwm4-themes
  apt install -y lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings gtk2-engines gtk3-engines-xfce system-config-printer plank
  apt install -y gnome-system-tools catfish pk-update-icon xfce4-indicator-plugin xfce4-messenger-plugin xfce4-radio-plugin xfce4-screenshooter-plugin xfce4-topmenu-plugin
fi

#[ Mate ]
if [ "$choix_environnement" = "3" ] ; then
  apt install -y mate-desktop-environment mate-desktop-environment-extras mate-applets mate-backgrounds mate-themes
  apt install -y lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings system-config-printer plank
fi

#[ Gnome 3 ]
if [ "$choix_environnement" = "4" ] ; then
  apt install -y gnome-shell gnome-session gnome-terminal gnome-themes gnome-themes-extras gnome-extra-icons gnome-system-monitor gnome-shell-extensions gnome-shell-extension-weather gnome-backgrounds
  apt install -y gdm3 gnome-tweak-tool file-roller gedit
fi

#[ Plasma 5 ]
if [ "$choix_environnement" = "5" ] ; then
  apt install -y kde-plasma-desktop sddm kde-config-gtk-style gtk2-engines-oxygen gtk3-engines-oxygen breeze
fi

#[ Cinnamon ]
if [ "$choix_environnement" = "6" ] ; then
  apt install -y cinnamon cinnamon-l10n cinnamon-screensaver gnome-terminal lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings
fi

######################################
# Installation des logiciels de base #
######################################

#[ Bureautique ]
apt install -y libreoffice libreoffice-gtk libreoffice-l10n-fr libreoffice-style-breeze libreoffice-style-sifr calibre
echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | /usr/bin/debconf-set-selections | apt install -y ttf-mscorefonts-installer 

# Backportage de LibreOffice (si demandé) => Pas encore actif, le dépot backports sera activé une fois la sortie officielle de deb9
#if [ "$choix_libreoffice" = "2" ]; then #via dépot backport
#  apt install -t stretch-backports -y libreoffice libreoffice-l10n-fr
#fi

#[ Internet / Web ]
apt install -y firefox-esr firefox-esr-l10n-fr chromium chromium-l10n flashplugin-nonfree thunderbird thunderbird-l10n-fr pidgin hexchat 
#firefox firefox-l10n-fr #<== plus tard pour backport mozilla
#apt install -y pepperflashplugin-nonfree midori 

#[ Download ]
apt install -y wget filezilla

#[ Video / Audio ]
apt install -y vlc x264 x265 kazam
#apt install -y audacity kazam banshee

#[ Graphisme / Photo ]
apt install -y gimp pinta shutter inkscape

#[ Utilitaires / Systeme ]
apt install -y numlockx htop gparted vim unrar hdparm p7zip zip file-roller keepassx grsync gnome-calculator
apt install -y screen ncdu alacarte net-tools gdebi software-properties-common synaptic
apt install -y thefuck
#apt install -y keepass2 glances virtualbox virtualbox-qt

# Virtualbox (dernière version stable)
echo "deb http://download.virtualbox.org/virtualbox/debian stretch contrib" >> /etc/apt/sources.list
wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | apt-key add -
apt update
apt install -y linux-headers-$(uname -r)
apt install -y virtualbox-5.1

#[ Développement / Programmation ]
apt install -y git openjdk-8-jdk geany
#apt install -y emacs codeblocks jedit ## <== paquet a vérifier !

# Spécifique Perso
if [ "$choix_profil" = "1" ]; then
  #[ Internet ]
  apt install -y pidgin hexchat
  
  #[ Audio/Vidéo ]
  apt install -y flac lame winff handbrake
  
  #[ Download ]
  apt install -y deluge transmission-gtk rtorrent filezilla 
  
  #[ Outil ]
  apt install -y keepassx asciinema screenfetch #encfs sirikali
  
  #[ Sciences / Education ]
  #apt install -y celestia
  
  #[ Serveur ]
  #apt install -y openssh-server
fi

# Spécifique Technicien/Assistance
if [ "$choix_profil" = "2" ]; then
  apt install -y keepassx keepass2 zim filezilla remmina
fi

####################################
# Ajout de logiciel supplémentaire #
####################################

for i in $choix_supplement; do  
  if [ "$i" = "2" ]; then #Oracle Java 
    echo "deb http://ppa.launchpad.net/webupd8team/java/ubuntu xenial main" > /etc/apt/sources.list.d/oracle-java.list
    apt install -y dirmngr
    apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys EEA14886
    apt update
    apt install -y oracle-java8-installer
  fi

  if [ "$i" = "3" ]; then #TeamViewer V12 (Pour utilisation perso)
    wget https://download.teamviewer.com/download/teamviewer_i386.deb
    dpkg -i teamviewer_i386.deb
    apt install -fy
  fi
  
  if [ "$i" = "4" ]; then #Teamviewer V8 (Pour technicien assistance)
    wget https://download.teamviewer.com/download/version_8x/teamviewer_linux.deb
    dpkg -i teamviewer_linux.deb
    apt install -fy
  fi

  if [ "$i" = "5" ]; then #Google Earth x64
    wget https://dl.google.com/dl/earth/client/current/google-earth-stable_current_amd64.deb
    dpkg -i google-earth-stable_current_amd64.deb
    apt install -fy
  fi

  if [ "$i" = "6" ]; then #Pack devéloppeur
    # Android Studio
    apt install -y lib32stdc++6 
    wget https://dl.google.com/dl/android/studio/ide-zips/2.3.2.0/android-studio-ide-162.3934792-linux.zip #(22/05/17)
    #wget https://dl.google.com/dl/android/studio/ide-zips/2.2.3.0/android-studio-ide-145.3537739-linux.zip #ancien
    unzip android-studio*
    mv android-studio /opt/
    wget https://raw.githubusercontent.com/sibe39/scripts_divers/master/android-studio.desktop
    mv android-studio.desktop /usr/share/applications/
    rm -rf android-studio* #suppresion de l'archive de téléchargement qui n'est plus utile
  
    # Autres applis
    apt install -y bluefish emacs
    
    # Sublime Text
    wget https://download.sublimetext.com/sublime-text_build-3126_amd64.deb
    dpkg -i sublime-text_build-3126_amd64.deb
    apt install -fy
    rm -f sublime-text_build-3126_amd64.deb
  fi

  if [ "$i" = "7" ]; then #Google Chrome
    wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
    dpkg -i google-chrome-stable_current_amd64.deb
    apt install -fy
  fi

  if [ "$i" = "8" ]; then #Client Hubic
    wget http://mir7.ovh.net/ovh-applications/hubic/hubiC-Linux/2.1.0/hubiC-Linux-2.1.0.53-linux.deb
    dpkg -i hubiC-Linux-2.1.0.53-linux.deb
    apt install -fy
  fi

  if [ "$i" = "9" ]; then #Minecraft
    wget https://raw.githubusercontent.com/sibe39/scripts_divers/master/install_minecraft.sh
    chmod +x install_minecraft.sh
    ./install_minecraft.sh
  fi

  if [ "$i" = "10" ]; then #Lecture DVD
    apt install -y libdvdread4
    wget http://download.videolan.org/pub/debian/testing/libdvdcss2_1.2.13-0_amd64.deb
    dpkg -i libdvdcss2_1.2.13-0_amd64.deb
    apt install -fy
  fi
  
  #if [ "$i" = "11" ]; then #Pour activation Snappy & flatpak
  #  mkdir /home/.snap
  #  ln -s /home/.snap /snap
    apt install -y snapd flatpak
  #fi
  
  if [ "$i" = "12" ]; then #Pour activer Compton
    apt install -y compton
    wget https://raw.githubusercontent.com/sibe39/scripts_divers/master/.compton.conf
    mv -f .compton.conf /home/$mon_login/.compton.conf
    #activer au démarrage dans votre environnement en mettant : compton -b
  fi
  
  if [ "$i" = "13" ]; then #Pack gaming
    apt install -y steam playonlinux wine supertux supertuxkart minetest openarena
  fi  
 
  if [ "$i" = "14" ]; then #Skype  
    wget https://repo.skype.com/latest/skypeforlinux-64.deb
    dpkg -i skypeforlinux-64.deb
    apt install -fy
  fi  
  
  if [ "$i" = "15" ]; then #PyCharm
    wget https://download-cf.jetbrains.com/python/pycharm-community-2016.3.2.tar.gz
    tar -zxvf pycharm* -C /opt/
    #a faire manuellement sans sudo la première fois => /opt/pycharm/bin/pycharm.sh
  fi 
  
  if [ "$i" = "100" ]; then #Supplément personnalisé pour Asus 15" (Simon)
    # Driver carte wifi ASUS 15"
    apt install -y linux-headers-$(uname -r)
    wget https://github.com/neurobin/MT7630E/archive/release.zip
    unzip release.zip
    cd MT7630E-release/
    chmod +x install test uninstall
    make dkms #ou ./install
    cd ..
    rm -f release.zip
  fi  
done

###################################
# Installation Driver sur demande #
###################################
if [ "$choix_driver" = "2" ]; then #{Si Carte nvidia unique avec pilote propriétaire récent} #a vérifier
  apt install -y linux-headers-$(uname -r) 
  apt install -y nvidia-detect nvidia-kernel-dkms nvidia-glx mesa-utils
  mkdir /etc/X11/xorg.conf.d
  echo -e 'Section "Device"\n\tIdentifier "My GPU"\n\tDriver "nvidia"\nEndSection' > /etc/X11/xorg.conf.d/20-nvidia.conf
fi

if [ "$choix_driver" = "3" ]; then #{Si chipset Intel unique avec pilote opensource}
  apt install -y linux-headers-$(uname -r) 
  apt install -y xserver-xorg-video-intel
fi

if [ "$choix_driver" = "4" ]; then #{Si technologie Optimus avec carte Nvidia (pilote propriétaire) et Chipset Intel (pilote opensource)}
  apt install -y linux-headers-$(uname -r)
  apt install -y xserver-xorg-video-intel bumblebee-nvidia primus primus-libs:i386 primus-libs-ia32
fi

if [ "$choix_driver" = "5" ]; then #{Si virtualisé avec Virtualbox (arch installé en tant que vm invité)}
  apt install -y linux-headers-$(uname -r)
  apt install -y virtualbox-guest-iso
fi

if [ "$choix_driver" = "6" ]; then #{Si Carte nvidia unique avec pilote propriétaire ancien}
  apt install -y linux-headers-$(uname -r)
  apt install -y nvidia-detect nvidia-legacy-340xx-driver
fi

########################
# Optimisation du swap #        
########################
echo vm.swappiness=5 | tee /etc/sysctl.d/99-swappiness.conf
sysctl -p /etc/sysctl.d/99-swappiness.conf

######################
# Customisation Xfce #        
######################
if [ "$choix_environnement" = "2" ]; then
  # Thème GTK
  apt install -y qt4-qtconfig numix-gtk-theme numix-icon-theme 
  apt install -y arc-theme human-icon-theme xfwm4-theme-breeze shiki-colors-xfwm-theme gnome-brave-icon-theme

  # Icones supplémentaires (numix circle)
  git clone https://github.com/numixproject/numix-icon-theme-circle.git
  cp -rf numix-icon-theme-circle/Numix* /usr/share/icons/ 
  #création cache our les icones 
  gtk-update-icon-cache /usr/share/icons/Numix-Circle/

  # Ecran de veille supplémentaire
  apt install -y xscreensaver-data-extra xscreensaver-gl-extra xscreensaver-screensaver-bsod

  # Curseur
  apt install -y dmz-cursor-theme

  # Custom menu Whisker de Xfce
  echo 'style "whisker-menu-numix-dark-theme"
  {
  base[NORMAL] = "#2B2B2B"
  base[ACTIVE] = "#D64937"
  text[NORMAL] = "#ccc"
  text[ACTIVE] = "#fff"
  bg[NORMAL] = "#2B2B2B"
  bg[ACTIVE] = "#D64937"
  bg[PRELIGHT] = "#D64937"
  fg[NORMAL] = "#ccc"
  fg[ACTIVE] = "#fff"
  fg[PRELIGHT] = "#fff"
  }
  widget "whiskermenu-window*" style "whisker-menu-numix-dark-theme"' >> /home/$mon_login/.gtkrc-2.0 

  # Custom gestionnaire de session LightDM
  mkdir temp ; cd temp/
  wget https://github.com/sibe39/img/blob/master/wp_251116.zip?raw=true
  unzip wp_251116.zip?raw=true ; rm -f wp_251116.zip\?raw\=true
  mv -f * /usr/share/backgrounds/xfce/ 
  #echo "[greeter]" >> /etc/lightdm/lightdm-gtk-greeter.conf 
  #echo "background=/usr/share/backgrounds/xfce/lightdm_wp02.jpg" >> /etc/lightdm/lightdm-gtk-greeter.conf

  # Uniquement pour utilisation "perso" ou "familiale", désactiver pour utilisation "pro" :
  # Récupération du fichier de config standard afin d'avoir l'userlist (pour ne pas entrer le login manuellement)
  wget https://raw.githubusercontent.com/sibe39/scripts_divers/master/lightdm.conf
  mv -f lightdm.conf /etc/lightdm/
fi

# Astuces diverses (désactivé par défaut)

# Pour désactiver les bips système 
# Ajouter a l'avant-dernière ligne du fichier /etc/rc.local :
# rmmod pcspkr


# Nettoyage pour finalisation
apt install -fy
apt autoremove --purge -y
apt clean -y

# Eteindre ?
echo "C'est terminé ! Un reboot est conseillé..."
read -p "Voulez-vous redémarrer immédiatement ? [O/n] " choix_reboot
if [ "$choix_reboot" = "O" ] || [ "$choix_reboot" = "o" ] || [ "$choix_reboot" = "" ] ; then
  reboot
fi
