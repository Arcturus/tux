#!/bin/bash
# version 1.2

###############################################################################
# SCRIPT POST-INSTALL (x)Ubuntu 16.04 Xenial Xerus & dérivés (ex : Mint 18.X) #
###############################################################################

# Condition minimum requise avant de lancer ce script :
# - Avoir une distribution basé sur la version 16.04 d'Ubuntu (exemple : Ubuntu 16.04, Xubuntu 16.04, Linux Mint 18.2, Elementary OS 0.4 etc...)

##################################################################
# Vérification que le script est bien lancé avec les droits admin
##################################################################
if [ "$UID" -ne "0" ]
then
  echo "Vous n'avez pas les bons droits, merci de lancer le script avec sudo !"
  exit 
fi 

# Pour identifier le numéro de la version (14.04, 16.04...)
. /etc/lsb-release

# attribution d´une variable pour la version utilisé
if [ "$DISTRIB_RELEASE" = "16.04" ] || [ "$DISTRIB_RELEASE" = "18" ] || [ "$DISTRIB_RELEASE" = "18.1" ] || [ "$DISTRIB_RELEASE" = "18.2" ] || [ "$DISTRIB_RELEASE" = "18.3" ] || [ "$DISTRIB_RELEASE" = "0.4" ] ; then
  version=xenial
fi

########################################################################
#vérification de la bonne version d'Ubuntu
########################################################################

if [ "$version" != "xenial" ] ; then
  echo "Vous n'êtes pas sûr une version compatible : seul une base 16.04LTS (xenial) est accepté ! (Ubuntu 16.04, Xubuntu 16.04, Mint 18.2, Elementary OS 0.4...)"
  exit
fi

# Question 1
echo "======================================="
echo "1/ Veuillez choisir le profil de base a utiliser"
echo "======================================="
echo "1 = Utilisation perso (Simon)"
echo "2 = Uitilisation Familiale"
echo "3 = Utilisation professionnelle : Technicien/Developpeur"
echo "4 = Utilisation professionnelle : Etablissement Scolaire"
echo "5 = Profil alternatif sur demande (inactif pour l´instant)"
echo "======================================="
read -p "Répondre par le chiffre correspondant (1 a 5) : " choix_profil
clear

# Question 1.1 (uniquement si l´utilisateur a choisi le ´profil scolaire´ a la question précédente)
if [ "$choix_profil" = "4" ] ; then
  echo "1.1/ Activer l'extinction automatique des postes le soir ?"
  echo "======================================="
  echo "1 = Non (ou déjà activé par le script IntegrDom)"
  echo "2 = oui, extinction a 18H30" 
  echo "3 = oui, extinction a 19H00"
  echo "4 = oui, extinction a 20H00"
  echo "5 = oui, extinction a 22H00"
  echo "======================================="
  read -p "Répondre par le chiffre correspondant (1 a 5) : " choix_extinction
fi
clear

# Question 2
echo "======================================="
echo "2/ Faut t'il backporter LibreOffice ?"
echo "======================================="
echo "1 = Non, conserver la version des dépots de Xenial (cad la branche 5.1)"
echo "2 = Oui, je veux la dernière version avec le PPA générique (5.3 et suivante)"
echo "======================================="
read -p "Répondre par le chiffre correspondant (1 ou 2) : " choix_libreoffice
clear

# Question 3
echo "======================================="
echo "3/ Un ou plusieurs logiciels supplémentaires a installer ?"
echo "======================================="
echo "1 = Non, aucun logiciel supplémentaire"
echo "2 = Version propriétaire de Java (Oracle Java)"
echo "3 = Teamviewer V12 : dernière version possible"
echo "4 = Teamviewer V8 (pour assistance pédagogique uniquement)"
echo "5 = Google Earth x64 (uniquement sur une installation 64 bits)"
echo "6 = Android Studio"
echo "7 = Google Chrome X64"
echo "8 = Client Hubic (a configurer vous-même après)"
echo "9 = Minecraft"
echo "10 = Activer HWE Stack Xenial (kernel & Xorg upgrade LTS.2/.3/.4)"
echo "11 = Celestia X64"
echo "12 = Paquets pour la lecture de DVD commerciaux"
echo "13 = Skype for Linux X64 (beta v5.3)"
echo "14 = Logiciels de dev : Emacs + Bluefish + Sublime-text"
echo "15 = Optimisation du Swap (inutile si vs n´avez pas de swap)"
echo "100 = Supplément perso pour vous (a modifier vous même dans le script avant)"
echo "======================================="
read -p "Répondre par le ou les chiffres correspondants séparés d'un espace (exemple : 2 5 6) : " choix_supplement
clear

# Question 4
echo "======================================="
echo "4/ Faut t'il installer/backporter VirtualBox ?"
echo "======================================="
echo "1 = Non, ne pas l´installer, je ne l´utilise pas"
echo "2 = Oui, l´installer mais pas backporté (version des dépots donc branche 5.0)"
echo "3 = Oui, l´installer & backporté (donc tj la dernière version : 5.1 et suivante)"
echo "======================================="
read -p "Répondre par le chiffre correspondant (1 ou 2 ou 3) : " choix_virtualbox
clear


# Question 5 #désactivé pour l´instant
#echo "======================================="
#echo "5/ Un (ou plusieurs) paquet(s) Snappy a installer ?"
#echo "======================================="
#echo "1 = Non"
#echo "2 = snappy:vlc (version de dévekoppement dailybuild : 3.0.0dev)"
#echo "3 = snappy:krita"
#echo "4 = snappy:youtube-dl-bdmurray"
#echo "5 = snappy:blender-tpaw (dernière version)"
#echo "======================================="
#read -p "Répondre par le ou les chiffres correspondants séparés d'un espace (exemple : 2 3) : " choix_snap
#clear


# Ajout dépot partenaire (sauf pour Mint ou c'est déjà le cas)
if [ "$(which mintupdate)" != "/usr/bin/mintupdate"  ] ; then 
  echo "deb http://archive.canonical.com/ubuntu xenial partner" >> /etc/apt/sources.list
fi

# Vérification que le système est a jour 
apt update
apt full-upgrade -y

# Spécifique environnement de bureau

# Ubuntu/Unity
if [ "$(which unity)" = "/usr/bin/unity" ] ; then 
  apt install -y ubuntu-restricted-extras ubuntu-restricted-addons unity-tweak-tool nautilus-image-converter nautilus-script-audio-convert
fi

# Xfce
if [ "$(which xfwm4)" = "/usr/bin/xfwm4" ] ; then
  apt install -y xfce4 xubuntu-restricted-extras xubuntu-restricted-addons xfce4-goodies xfwm4-themes gtk3-engines-xfce plank 
fi

# Mate
if [ "$(which caja)" = "/usr/bin/caja" ] ; then 
  apt install -y mate-desktop-environment-extra
fi

# Lxde
if [ "$(which pcmanfm)" = "/usr/bin/pcmanfm" ] ; then 
  apt install -y lubuntu-restricted-extras lubuntu-restricted-addons 
fi

####################################################
# Installation des logiciels de base (pour tous !) #
#################################################### 

#[ Bureautique ]
if [ "$choix_libreoffice" = "2" ] ; then
  add-apt-repository -y ppa:libreoffice/ppa ; apt update
fi

apt install -y libreoffice libreoffice-l10n-fr libreoffice-style-breeze

# Police d'écriture (ms) supplémentaire
echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | /usr/bin/debconf-set-selections | apt install -y ttf-mscorefonts-installer 

#[ Internet / Web ]
apt install -y firefox firefox-locale-fr chromium-browser chromium-browser-l10n adobe-flashplugin thunderbird thunderbird-locale-fr pidgin  

#[ Video / Audio ]
apt install -y vlc 

#[ Graphisme / Photo ]
apt install -y gimp pinta shutter

#[ Utilitaires / Systeme ]
apt install -y htop gparted vim unrar numlockx gdebi synaptic

#[ Développement / Programmation ]
apt install -y openjdk-8-jdk

# Virtualbox 
if [ "$choix_virtualbox" = "2" ] ; then
  apt install -y virtualbox virtualbox-qt
  else if [ "$choix_virtualbox" = "3" ] ; then
    echo "deb http://download.virtualbox.org/virtualbox/debian xenial contrib" >> /etc/apt/sources.list.d/virtualbox.list
    wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | apt-key add -
    apt update
    apt install -y linux-headers-$(uname -r)
    apt install -y virtualbox-5.1
  fi
fi

# Spécifique Perso
if [ "$choix_profil" = "1" ]; then

  #[ Bureautique ]
  apt install -y calibre #(lecture ebook)
  
  #[ Vidéo/Audio ]
  apt install -y x264 x265 flac lame handbrake
  
  #[ Internet ]
  apt install -y hexchat
  
  #[ Dev ]
  apt install -y geany git
  
  #[ Download ]
  apt install -y deluge transmission-gtk filezilla
  
  #[ Outil ]
  apt install -y keepassx bleachbit asciinema screenfetch hdparm screen ncdu
  
  # GEM
  add-apt-repository -y ppa:gencfsm/ppa
  apt update
  apt install -y gnome-encfs-manager
  
  #Kclean
  wget http://hoper.dnsalias.net/tdc/public/kclean.deb
  dpkg -i kclean.deb
  apt install -fy
  rm -f kclean.deb
  
  # Blacklistage de Acer-WMI pour le wifi du Xiaomi Air 13 (inutile si kernel > 4.9)
  echo blacklist acer-wmi | tee -a /etc/modprobe.d/blacklist-acer-wmi.conf
  
fi

# Spécifique Famille

if [ "$choix_profil" = "2" ]; then
  apt install -y keepassx openssh-server pidgin transmission-gtk
  
  #Imprimante HP + scanner
  apt install -y hplip hplip-gui sane
  
fi

# Spécifique Technicien/Assistance

if [ "$choix_profil" = "3" ]; then
  apt install -y keepassx keepass2 zim filezilla kazam
fi

# Spécifique Scolaire

if [ "$choix_profil" = "4" ]; then

  #[ Bureautique ]
  apt install -y freeplane scribus gnote xournal cups-pdf #planner
  
  #[ Video / Audio ]
  apt install -y imagination openshot audacity ffmpeg2theora flac vorbis-tools lame oggvideotools mplayer ogmrip goobox #winff

  #[ Graphisme / Photo ]
  apt install -y blender sweethome3d gthumb mypaint hugin inkscape

  #[ Systeme ]
  apt install -y pyrenamer rar unrar diodon p7zip-full

  #[ Mathematiques ]
  apt install -y geogebra algobox carmetal scilab

  #[ Sciences ]
  apt install -y stellarium avogadro #optgeo

  #[ Programmation ]
  apt install -y scratch ghex geany imagemagick idle-python3.5 
  
  #[ Drivers ]
  # Imprimantes
  wget http://www.openprinting.org/download/printdriver/debian/dists/lsb3.2/contrib/binary-amd64/openprinting-gutenprint_5.2.7-1lsb3.2_amd64.deb
  dpkg -i openprinting-gutenprint_5.2.7-1lsb3.2_amd64.deb 
  apt install -fy
  # Scanner les plus courants
  apt install -y sane
fi

# Profil personnalisé alternatif, a modifier plus tard
if [ "$choix_profil" = "2" ]; then 
  apt install -y keepass2 
  #..........
fi



####################################
# Ajout de logiciel supplémentaire #
####################################

for i in $choix_supplement; do  

  if [ "$i" = "2" ]; then #Oracle Java
    add-apt-repository -y ppa:webupd8team/java
    apt update
    echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections | apt install -y oracle-java8-installer
  fi

  if [ "$i" = "3" ]; then #TeamViewer V12
    wget https://download.teamviewer.com/download/teamviewer_i386.deb
    dpkg -i teamviewer_i386.deb
    apt install -fy
  fi
  
  if [ "$i" = "4" ]; then #Teamviewer V8 (technicien)
    wget http://download.teamviewer.com/download/version_8x/teamviewer_linux.deb
    dpkg -i teamviewer_linux.deb
    apt install -fy
  fi

  if [ "$i" = "5" ]; then #Google Earth x64
    wget https://dl.google.com/dl/earth/client/current/google-earth-stable_current_amd64.deb
    dpkg -i google-earth-stable_current_amd64.deb
    apt install -fy
  fi

  if [ "$i" = "6" ]; then #Android Studio
    # Android Studio
    add-apt-repository -y ppa:paolorotolo/android-studio
    apt update
    apt install -y android-studio
  fi

  if [ "$i" = "7" ]; then #Google Chrome
    wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
    dpkg -i google-chrome-stable_current_amd64.deb
    apt install -fy
  fi

  if [ "$i" = "8" ]; then #Client Hubic
    wget http://mir7.ovh.net/ovh-applications/hubic/hubiC-Linux/2.1.0/hubiC-Linux-2.1.0.53-linux.deb
    dpkg -i hubiC-Linux-2.1.0.53-linux.deb
    apt install -fy
  fi

  if [ "$i" = "9" ]; then #Minecraft
    wget https://raw.githubusercontent.com/sibe39/scripts_divers/master/install_minecraft.sh
    chmod +x install_minecraft.sh
    ./install_minecraft.sh
    rm -f install_minecraft.sh
  fi
  
  if [ "$i" = "10" ]; then #HWE Stack
    apt -y install --install-recommends xserver-xorg-hwe-16.04
  fi
  
  if [ "$i" = "11" ]; then #Installation Celestia
    wget https://raw.githubusercontent.com/sibe39/scripts_divers/master/Celestia_On_Xenial.sh
    chmod +x Celestia_On_Xenial.sh
    ./Celestia_On_Xenial.sh
    rm -f Celestia_On_Xenial.sh
  fi  
  
  if [ "$i" = "12" ]; then #Lecture dvd commerciaux
    apt install -y libdvdread4
    wget http://download.videolan.org/pub/debian/testing/libdvdcss2_1.2.13-0_amd64.deb
    dpkg -i libdvdcss2_1.2.13-0_amd64.deb
    apt install -fy
  fi
  
  if [ "$i" = "13" ]; then #Skype
    #apt install -y skype (vieille version dans les dépots de Xenial)
    wget https://repo.skype.com/latest/skypeforlinux-64.deb ; dpkg -i skypeforlinux-64.deb ; apt install -fy ; rm -f skypeforlinux-64.deb ;
  fi 
  
  if [ "$i" = "14" ]; then #emacs/bluefish/sublime-text
    apt install -y emacs bluefish sublime-text
  fi      
  
  if [ "$i" = "15" ]; then Optimisation swap
    echo vm.swappiness=5 | tee /etc/sysctl.d/99-swappiness.conf
    sysctl -p /etc/sysctl.d/99-swappiness.conf
  fi
  
  if [ "$i" = "100" ]; then #Supplément personnalisé ( MODIFIER ICI POUR VOTRE PROPRE CONTENU )
  
    ## Specifique ancien PC asus 15 pouces Simon (pour wifi)
    apt install -y linux-headers-$(uname -r)
    wget https://github.com/neurobin/MT7630E/archive/release.zip
    unzip release.zip
    cd MT7630E-release/
    chmod +x install test uninstall
    make dkms #ou ./install
    cd ..
  fi  
done

# Extinction automatique si demandé

if [ "$choix_extinction" = "2" ] ; then
        echo "30 18 * * * root /sbin/shutdown -h now" > /etc/cron.d/prog_extinction
        else if [ "$choix_extinction" = "3" ] ; then
                echo "0 19 * * * root /sbin/shutdown -h now" > /etc/cron.d/prog_extinction
                else if [ "$choix_extinction" = "4" ] ; then
                        echo "0 20 * * * root /sbin/shutdown -h now" > /etc/cron.d/prog_extinction
                        else if [ "$choix_extinction" = "5" ] ; then
                                echo "0 22 * * * root /sbin/shutdown -h now" > /etc/cron.d/prog_extinction
                             fi
                     fi
             fi
fi

########################################################################
#suppression de l'envoi des rapport d'erreurs
########################################################################
echo "enabled=0" > /etc/default/apport


# Nettoyage pour finalisation
apt install -fy
apt autoremove --purge -y
apt clean -y


# Eteindre ?
echo "C'est terminé ! Un reboot est conseillé..."
read -p "Voulez-vous redémarrer immédiatement ? [O/n] " choix_reboot
if [ "$choix_reboot" = "O" ] || [ "$choix_reboot" = "o" ] || [ "$choix_reboot" = "" ] ; then
  reboot
fi
