#!/bin/bash
# version 1.5

#################################
# SCRIPT POST-INSTALL ARCHLINUX
#################################

# Condition minimum requise avant de lancer ce script :
# - Avoir Archlinux d'installé (système de base + serveur graphique + pilote graphique)
# - Le dépot "Multilib" doit être activé (/etc/pacman.conf) et pas d'autres dépots facultatif ajoutés (fais par le script)
# (il n'est pas obligatoire d'avoir déjà un environneemnt de bureau déjà installé)

##################################################################
# Vérification que le script est bien lancé avec les droits admin
##################################################################
if [ "$UID" -ne "0" ]
then
  echo "Vous n'avez pas les bons droits, merci de lancer le script avec sudo !"
  exit 
fi 

# Question 1
echo "======================================="
echo "Faut t'il installer un environnement graphique de bureau ?"
echo "======================================="
echo "1 = non, j'ai déja un environnement de bureau"
echo "2 = Xfce avec extra (+LightDM)"
echo "3 = Mate avec extra (+LightDM)"
echo "4 = Gnome Shell/Gnome 3 avec extra (+GDM3)"
echo "5 = Kde/Plasma 5 (+SDDM)"
echo "6 = Cinnamon (+LightDM)"
echo "======================================="
read -p "Répondre par le chiffre correspondant (1 a 6) : " choix_environnement
clear

# Question 2
echo "======================================="
echo "Faut t'il installer les drivers propriétairess ?"
echo "======================================="
echo "2 = Oui, driver propriétaire nvidia (carte nvidia seule)"
echo "3 = Oui, driver opensource+propriétaire intel (carte intel seule)"
echo "4 = Oui, technologie Optimus : carte nvidia + chipset nvidia (bumblebee)"
echo "5 = Additions invités pour VirtualBox"
echo "======================================="
read -p "Répondre par le chiffre correspondant (1 a 5) : " choix_driver
clear

# Question 3
echo "======================================="
echo "Identifiant sous Archlinux" 
echo "======================================="
read -p "Merci de saisir votre login utilisateur : " mon_login
clear

# Question 4
echo "======================================="
echo "Un (ou plusieurs) logiciels supplémentaires a installer ?"
echo "======================================="
echo "1 = Non"
echo "2 = Oracle Java (version propriétaire) [AUR]"
echo "3 = Teamviewer : dernière version (V12 a ce jour) [AUR]"
echo "5 = Google Earth x64 [AUR]"
echo "6 = Android Studio [AUR]"
echo "7 = Google Chrome [AUR]"
echo "8 = Client Hubic [AUR]"
echo "9 = Minecraft [AUR]"
echo "10 = Sirikali [AUR]"
echo "100 = Supplément perso Simon (driver wifi asus15)"
echo "======================================="
read -p "Répondre par le ou les chiffres correspondants séparés d'un espace (exemple : 2 5 6) : " choix_supplement
clear

# Vérification que le système est a jour 
pacman --noconfirm -Syyu

#############################################
# Installation de l'environnement de bureau #
#############################################

# Xfce
if [ "$choix_environnement" = "2" ] ; then
  pacman --noconfirm -S xfce4 xfce4-whiskermenu-plugin xfce4-terminal xfce4-artwork xfce4-weather-plugin lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings light-locker plank catfish
fi

# Mate
if [ "$choix_environnement" = "3" ] ; then
  pacman --noconfirm -S mate mate-extra mate-accountsdialog mate-color-manager mate-applets mate-backgrounds mate-themes lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings plank
fi

# Gnome 3
if [ "$choix_environnement" = "4" ] ; then
  pacman --noconfirm -S gnome gnome-terminal file-roller gedit gdm gnome-tweak-tool gnome-system-monitor gnome-shell-extensions gnome-backgrounds
fi

# Plasma 5
if [ "$choix_environnement" = "5" ] ; then
  pacman --noconfirm -S plasma sddm
fi

#[ Cinnamon ]
if [ "$choix_environnement" = "6" ] ; then
  pacman --noconfirm -S cinnamon cinnamon-l10n gnome-terminal lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings
fi

# Pouvoir installer facilement des paquets depuis le dépot communautaire 
pacman --noconfirm -S yaourt

######################################
# Installation des logiciels de base #
######################################

#[ Bureautique ]
pacman --noconfirm -S libreoffice-fresh libreoffice-fresh-fr ttf-liberation ttf-dejavu evince calibre
#yaourt -S libreoffice-breeze-icons ttf-ms-fonts ttf-ms-win10

#[ Web / Internet ]
pacman --noconfirm -S firefox firefox-i18n-fr chromium flashplugin thunderbird thunderbird-i18n-fr pidgin hexchat
#yaourt -S google-chrome skype skypeforlinux-bin tor-browser-en firefox-developer palemoon chromium-pepper-flash

#[ Download ]
pacman --noconfirm -S deluge transmission-gtk wget filezilla
#yaourt -S hubic

#[ Graphisme / Photo ]
pacman --noconfirm -S gimp pinta shutter 

#[ Vidéo / Audio ]
pacman --noconfirm -S vlc x264 x265 lame flac #sound-juicer
#pacman --noconfirm -S libdvdread libdvdnav libdvdcss
#yaourt -S kazam

#[ Sciences / Education ]
#pacman --noconfirm -S celestia
#yaourt -S google-earth

#[ Utilitaires / Système ]
pacman --noconfirm -S htop gparted vim unrar keepassx2 hdparm virtualbox virtualbox-qt sudo npm blueman bluez
pacman --noconfirm -S ntfs-3g screen ncdu screenfetch asciinema encfs numlockx grsync 
#A faire manuellement : yaourt -S sirikali ou Gnome-encfs-manager ou autre...

#pacman --noconfirm -S glances reflector keepass2 unetbootin

#[ Développement / Programmation ]
pacman --noconfirm -S git jdk8-openjdk 
#pacman --noconfirm -S emacs codeblocks jedit bluefish 
#yaourt -S android-studio visual-studio-code-oss jdk

#[ Serveur ]
#pacman --noconfirm -S openssh
#yaourt -S minecraft-server

#[ Jeux-Vidéo ]
#pacman --noconfirm -S steam minetest
#pacman --noconfirm -S 0ad
#yaourt -S minecraft

## Thème / Icon supplémentaire (indépendamment de l'environnement de bureau)

pacman --noconfirm -S numix-gtk-theme arc-gtk-theme arc-solid-gtk-theme breeze-gtk
pacman --noconfirm -S arc-icon-theme breeze-icons faience-icon-theme flattr-icon-theme human-icon-theme 
#yaourt -S papirus-icon-theme-git papirus-libreoffice-theme-git

# Numix circle
git clone https://github.com/numixproject/numix-icon-theme-circle.git
cp -rf numix-icon-theme-circle/Numix* /usr/share/icons/ 






####[MODIF SCRIPT] ajouter question
#{Si Carte nvidia unique avec pilote propriétaire}
#pacman --no-confirm -S nvidia nvidia-utils nvidia-libgl

#{Si chipset Intel unique avec pilote opensource}
#pacman --no-confirm -S xf86-video-intel

#{Si technologie Optimus avec carte Nvidia (pilote propriétaire) et Chipset Intel (pilote opensource)}
#pacman --noconfirm -S intel-dri xf86-video-intel bumblebee nvidia nvidia-utils lib32-nvidia-utils lib32-nvidia-utils bbswitch
#pacman --no-confirm -S mesa-demos lib32-mesa-demos
#usermod -a -G bumblebee {mon login}
#systemctl enable bumblebeed.service

#{Si virtualisé avec Virtualbox (arch installé en tant que vm invité)}
#pacman --noconfirm -S virtualbox-guest-utils
#echo "vboxguest" >> /etc/modules-load.d/virtualbox.conf
#echo "vboxsf" >> /etc/modules-load.d/virtualbox.conf
#echo "vboxvideo" >> /etc/modules-load.d/virtualbox.conf

####################################
# Ajout de logiciel supplémentaire #
####################################

for i in $choix_supplement; do  
# Connexion sans les droits admin pour Yaourt :
su $mon_login
  if [ "$i" = "2" ]; then #Oracle Java
    yaourt --noconfirm -S jdk
  fi

  if [ "$i" = "3" ]; then #TeamViewer V12
    yaourt --noconfirm -S teamviewer
  fi

  if [ "$i" = "5" ]; then #Google Earth x64
    yaourt --noconfirm -S google-earth
  fi

  if [ "$i" = "6" ]; then #Android Studio
    yaourt --noconfirm -S android-studio
  fi

  if [ "$i" = "7" ]; then #Google Chrome
    yaourt --noconfirm -S google-chrome
  fi

  if [ "$i" = "8" ]; then #Client Hubic
    yaourt --noconfirm -S hubic
  fi

  if [ "$i" = "9" ]; then #Minecraft
    yaourt --noconfirm -S minecraft-launcher
  fi
  
  if [ "$i" = "10" ]; then #sirikali 
    yaourt --noconfirm -S sirikali
  fi
  
  if [ "$i" = "11" ]; then #divers
    yaourt --noconfirm -S pacaur skypeforlinux-bin pepper-flash tor-browser-en ttf-ms-fonts pycharm-community numix-circle-icon-theme-git telegram-desktop 
  
  exit #<= pour revenir avec le compte root suite a su $mon_login
  
  if [ "$i" = "100" ]; then #Supplément personnalisé (Simon)
    #[Driver carte wifi ASUS 15]
    pacman --noconfirm -S linux-headers-$(uname -r)
    wget https://github.com/neurobin/MT7630E/archive/release.zip
    unzip release.zip
    cd MT7630E-release/
    chmod +x install test uninstall
    make dkms #ou ./install
    cd ..
    #Ou sans sudo manuellement : yaourt -S mt7630-pcie-wifi-dkms

    #[Touchpad Asus15]
    pacman --noconfirm -S xf86-input-synaptics
  fi  
done

###################################
# Installation Driver sur demande #     non vérifié !
###################################
if [ "$choix_driver" = "2" ]; then #{Si Carte nvidia unique avec pilote propriétaire}
  pacman --noconfirm -S nvidia
  mkdir /etc/X11/xorg.conf.d
  echo -e 'Section "Device"\n\tIdentifier "My GPU"\n\tDriver "nvidia"\nEndSection' > /etc/X11/xorg.conf.d/20-nvidia.conf
fi

if [ "$choix_driver" = "3" ]; then #{Si chipset Intel unique avec pilote opensource}
  pacman --noconfirm -S xf86-video-intel
fi

if [ "$choix_driver" = "4" ]; then #{Si technologie Optimus avec carte Nvidia (pilote propriétaire) et Chipset Intel (pilote opensource)}
  pacman --noconfirm -S linux-headers-$(uname -r)
  pacman --noconfirm -S mesa-dri xf86-video-intel bumblebee nvidia lib32-nvidia-utils lib32-mesa-libgl mesa-demos
  gpasswd -a $mon_login bumblebee 
  systemctl start bumblebeed.service
  systemctl enable bumblebeed.service
fi

if [ "$choix_driver" = "5" ]; then #{Si virtualisé avec Virtualbox (arch installé en tant que vm invité)}
  pacman --noconfirm -S linux-headers-$(uname -r)
  pacman --noconfirm -S virtualbox-guest-utils
fi

######################
# Customisation Xfce #       
######################
if [ "$choix_environnement" = "2" ]; then
  pacman --noconfirm -S qt4-qtconfig 

  # Ecran de veille supplémentaire
  pacman --noconfirm -S xscreensaver-data-extra xscreensaver-gl-extra xscreensaver-screensaver-bsod

  # Curseur
  pacman --noconfirm -S dmz-cursor-theme

  # Custom menu Whisker de Xfce
  echo 'style "whisker-menu-numix-dark-theme"
  {
  base[NORMAL] = "#2B2B2B"
  base[ACTIVE] = "#D64937"
  text[NORMAL] = "#ccc"
  text[ACTIVE] = "#fff"
  bg[NORMAL] = "#2B2B2B"
  bg[ACTIVE] = "#D64937"
  bg[PRELIGHT] = "#D64937"
  fg[NORMAL] = "#ccc"
  fg[ACTIVE] = "#fff"
  fg[PRELIGHT] = "#fff"
  }
  widget "whiskermenu-window*" style "whisker-menu-numix-dark-theme"' >> /home/$mon_login/.gtkrc-2.0 

  # Custom gestionnaire de session LightDM
  mkdir temp ; cd temp/
  wget https://github.com/sibe39/img/blob/master/wp_251116.zip?raw=true
  unzip wp_251116.zip?raw=true ; rm -f wp_251116.zip\?raw\=true
  mv -f * /usr/share/backgrounds/xfce/ 
  echo "[greeter]" >> /etc/lightdm/lightdm-gtk-greeter.conf 
  echo "background=/usr/share/backgrounds/xfce/lightdm_wp02.jpg" >> /etc/lightdm/lightdm-gtk-greeter.conf

  # Récupération du fichier de config standard afin d'avoir l'userlist (pour ne pas entrer le login manuellement)
  wget https://raw.githubusercontent.com/sibe39/scripts_divers/master/lightdm.conf
  mv -f lightdm.conf /etc/lightdm/
fi

########################
# Optimisation du swap #        
########################
echo vm.swappiness=5 | tee /etc/sysctl.d/99-swappiness.conf
sysctl -p /etc/sysctl.d/99-swappiness.conf

# Nettoyage pour finalisation
pacman --noconfirm -Sc

# Eteindre ?
echo "C'est terminé ! Un reboot est conseillé..."
read -p "Voulez-vous redémarrer immédiatement ? [O/n] " choix_reboot
if [ "$choix_reboot" = "O" ] || [ "$choix_reboot" = "o" ] || [ "$choix_reboot" = "" ] ; then
  reboot
fi
