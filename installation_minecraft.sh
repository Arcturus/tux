#!/bin/bash
# DOIT ETRE LANCE AVEC DROIT ADMIN (=> sudo) !

# Installation de Minecraft sous Linux + raccourci dans le menu

# Téléchargement du fichier java et mise en place

mkdir /usr/share/minecraft
wget http://s3.amazonaws.com/Minecraft.Download/launcher/Minecraft.jar
mv -f Minecraft.jar /usr/share/minecraft/minecraft.jar
chmod +x /usr/share/minecraft/minecraft.jar

# Création du raccourci dans le menu

echo "[Desktop Entry]
Type=Application
Name=Minecraft
GenericName=Game
Comment=Break and place blocks to build imaginative things
Exec=java -jar /usr/share/minecraft/minecraft.jar
Icon=/usr/share/minecraft/icon-minecraft.png
Categories=Game
StartupNotify=true
Actions=Screenshots;Resources;Wiki
StartupWMClass=net-minecraft-bootstrap-Bootstrap

[Desktop Action Screenshots]
Name=_Screenshots
Exec=xdg-open .minecraft/screenshots/

[Desktop Action Resources]
Name=_Resource Packs
Exec=xdg-open .minecraft/resourcepacks/

[Desktop Action Wiki]
Name=_Wiki
Exec=xdg-open http://www.minecraftwiki.net/" > /usr/share/applications/minecraft.desktop

# Icone (si pas présent)
wget http://www.pngfactory.net/_png/_thumb/22383-bubka-Minecraft.png
mv -f 22383-bubka-Minecraft.png /usr/share/minecraft/icon-minecraft.png

