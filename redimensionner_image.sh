#!/bin/bash

# Script a placer dans le dossier ou vous souhaitez redimensionner en masse les images
# Changer l'extension si besoin (actuellement png)
# Changer la dimension finale (actuellement 40x40)

for img in `ls *.png`;
do
	echo $img
    convert -thumbnail 40x40  $img `basename $img` ;
done
