#!/bin/bash

#######################################
IN="eDP1"       # écran pcportable
EXT="HDMI2"     # sortie hdmi
#######################################

# prérequis a faire 1 fois au début : touch ~/.script/hdmi_off dans répertoire "~/.script" 

if [ -f ~/.script/hdmi_off ] ; then

#Activation HDMI
xrandr --output $IN --off --output $EXT --auto
pacmd set-card-profile 0 output:hdmi-stereo #audio hdmi activé
pacmd set-card-profile 1 off   #audio pc désactivé
mv ~/.script/hdmi_off ~/.script/hdmi_on

else

#Désactivation HDMI
xrandr --output $EXT --off --output $IN --auto
pacmd set-card-profile 1 output:analog-stereo #audio pc activé (audio hdmi forcément désactivé)
mv ~/.script/hdmi_on ~/.script/hdmi_off


fi
